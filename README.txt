Author: 
Christian Romero
Restaurant Enterprise Management Utility
 
par History:

date 2020/02/25    Initial release

version 1.0.0

====================================================================

Author: 
Adolfo Mendiola, Tori Gardner
Restaurant Enterprise Management Utility
 
par History:

date 2020/02/25    Added suppliers functionality

version 1.0.1

====================================================================

Author: 
Adolfo Mendiola
Restaurant Enterprise Management Utility
 
par History:

date 2020/04/03    Added Tickets functionality

version 1.0.2
====================================================================

Author: 
Adolfo Mendiola
Restaurant Enterprise Management Utility
 
par History:

date 2020/04/03    Dropdown Menus show names + ID

version 1.0.3
====================================================================

Author: 
Tori Gardner
Restaurant Enterprise Management Utility
 
par History:

date 2020/04/04    CSS+ Orders

version 1.0.4
====================================================================

Author: 
Tori Gardner
Restaurant Enterprise Management Utility
 
par History:

date 2020/04/05    CSS Upgrades

version 1.0.5

