from django.shortcuts import render
from .itemsForm import ItemsForm
from .models import Items
from django.http import HttpResponse


def items_view(request):
    obj = Items.objects.get(itemid=1)
    form = ItemsForm(request.POST or None)

    if request.POST:

        if '_Delete' in request.POST:
            searchid = request.POST.get('itemid', None)
            Items.objects.filter(itemid=searchid).delete()
            return HttpResponse("Item: "+searchid + " was deleted")

        elif '_Update' in request.POST:
            searchid = request.POST.get('itemid', None)
            Items.objects.filter(itemid=searchid).delete()



    if form.is_valid():
        form.save()

    context = {
        'object': obj,
        'form': form,

    }

    return render(request, "items.html", context)