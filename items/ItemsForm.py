from django import forms
from django.forms import widgets

from .models import Items


class ItemsForm(forms.ModelForm):

    class Meta:
        model = Items
        fields = [
            'itemid',
            'name',
            'supplierid',
            'price',
            'pricetype',
            'measurementunit',
        ]
<<<<<<< HEAD
        widgets = {
=======
        widgets={
>>>>>>> 3590ab3cecf540a19c4977d398917cc213803941
            'itemid': forms.TextInput(attrs={'class': 'input'}),
            'name': forms.TextInput(attrs={'class': 'input'}),
            'supplierid': forms.TextInput(attrs={'class': 'input'}),
            'price': forms.NumberInput(attrs={'class': 'input'}),
            'pricetype': forms.TextInput(attrs={'class': 'input'}),
            'measurementunit': forms.TextInput(attrs={'class': 'input'})
        }

    def __init__(self, *args, **kwargs):
        super(ItemsForm, self).__init__(*args, **kwargs)
        self.fields['price'].disabled = True