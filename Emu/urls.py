"""EMU URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from pages.views import home_view
from items.views import items_view
from inventory.views import inventory_view
from suppliers.views import suppliers_view
from tickets.views import tickets_view
from tickets.views import ticketsDishes_view
from dishes.views import dishes_view
from dishes.views import recipes_view
from orders.views import orders_view


urlpatterns = [

    path('', home_view, name='home'),
    path('items/', items_view),
    path('inventory/', inventory_view),
    path('suppliers/', suppliers_view),
    path('tickets/', tickets_view),
    path('tickets/edit/', ticketsDishes_view),
    path('dishes/', dishes_view),
    path('dishes/edit/', recipes_view),
    path('orders/', orders_view),
    path('admin/', admin.site.urls),
]
