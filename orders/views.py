from django.shortcuts import render
from .models import Inventory
from .ordersForm import OrdersForm


def orders_view(request):

    form = OrdersForm(request.POST or None)

    if request.POST:



        if 'AddOrder' in request.POST:
            itemid = request.POST.get('itemid', None)
            addQuantity = request.POST.get('quantity', None)
            oldQuantity = Inventory.objects.get(itemid=itemid).quantity

            Inventory.objects.get(itemid=itemid).quantity = oldQuantity + addQuantity;

    if form.is_valid():
        form.save()

    context = {
        'form': form,
    }

    return render(request, "orders.html", context)





