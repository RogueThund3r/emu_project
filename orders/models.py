from django.db import models

class Supplier(models.Model):
    supplierid = models.IntegerField(db_column='SupplierID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50)  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=50)  # Field name made lowercase.
    phoneno = models.CharField(db_column='PhoneNo', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Supplier'


class Items(models.Model):
    itemid = models.CharField(db_column='ItemID', primary_key=True,max_length=3)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50)  # Field name made lowercase.
    supplierid = models.ForeignKey('Supplier', models.CASCADE, db_column='SupplierID')  # Field name made lowercase.
    price = models.DecimalField(db_column='Price', max_digits=19, decimal_places=2)  # Field name made lowercase.
    pricetype = models.CharField(db_column='PriceType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    measurementunit = models.CharField(db_column='MeasurementUnit', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Items'

class Inventory(models.Model):
    invid = models.IntegerField(db_column='InvID', primary_key=True)  # Field name made lowercase.
    itemid = models.ForeignKey('Items', models.CASCADE, db_column='ItemID')  # Field name made lowercase.
    datepurchased = models.DateField(db_column='DatePurchased')  # Field name made lowercase.
    shelflife = models.IntegerField(db_column='ShelfLife')  # Field name made lowercase.
    quantity = models.FloatField(db_column='Quantity')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Inventory'

class Orders(models.Model):
    orderid = models.IntegerField(db_column='OrderID', primary_key=True)  # Field name made lowercase.
    supplierid = models.ForeignKey('Supplier', models.CASCADE, db_column='SupplierID')  # Field name made lowercase.
    itemid = models.ForeignKey(Items, models.CASCADE, db_column='ItemID')  # Field name made lowercase.
    quantity = models.IntegerField(db_column='Quantity')  # Field name made lowercase.
    total = models.DecimalField(db_column='Total', max_digits=19, decimal_places=2)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Orders'


