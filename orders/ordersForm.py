from django import forms
from .models import Orders


class OrdersForm(forms.ModelForm):
    class Meta:
        model = Orders
        fields = [
            'orderid',
            'supplierid',
            'itemid',
            'quantity',
            'total',
         ]
        widgets = {
            'orderid': forms.TextInput(attrs={'class': 'input'}),
            'supplierid': forms.TextInput(attrs={'class': 'input'}),
            'itemid': forms.TextInput(attrs={'class': 'input'}),
            'quantity': forms.NumberInput(attrs={'class': 'input'}),
            'total': forms.TextInput(attrs={'class': 'input'}),
        }

    def __init__(self, *args, **kwargs):
        super(OrdersForm, self).__init__(*args, **kwargs)
        self.fields['total'].disabled = True