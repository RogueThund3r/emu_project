from django import forms
from .models import Ticket
from .models import TicketDishes


class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = [
            'ticketid',
            'date',
            'subtotal',
            'tip',
            'total',
        ]
        widgets = {
            'ticketid': forms.TextInput(attrs={'class': 'input'}),
            'date': forms.TextInput(attrs={'class': 'input'}),
            'subtotal': forms.TextInput(attrs={'class': 'input'}),
            'tip': forms.NumberInput(attrs={'class': 'input'}),
            'total': forms.TextInput(attrs={'class': 'input'}),
        }


class TicketDishesForm(forms.ModelForm):
    class Meta:
        model = TicketDishes
        fields = [
            'ticketid',
            'dishid',
            'quantity',
        ]
        widgets = {
            'ticketid': forms.TextInput(attrs={'class': 'input'}),
            'dishid': forms.TextInput(attrs={'class': 'input'}),
            'quantity': forms.TextInput(attrs={'class': 'input'}),
        }