from django import forms
from .models import Dishes
from .models import Recipes


class DishesForm(forms.ModelForm):
    class Meta:
        model = Dishes
        fields = [
            'dishid',
            'name',
            'price',
            'type',
        ]
        widgets = {
            'dishid': forms.TextInput(attrs={'class': 'input'}),
            'name': forms.TextInput(attrs={'class': 'input'}),
            'price': forms.NumberInput(attrs={'class': 'input'}),
            'type': forms.TextInput(attrs={'class': 'input'}),
        }


class RecipesForm(forms.ModelForm):
    class Meta:
        model = Recipes
        fields = [
            'dishid',
            'itemid',
            'quantity',
        ]
        widgets = {
            'dishid': forms.TextInput(attrs={'class': 'input'}),
            'itemid': forms.TextInput(attrs={'class': 'input'}),
            'quantity': forms.NumberInput(attrs={'class': 'input'}),
        }