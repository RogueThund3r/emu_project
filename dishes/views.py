from django.shortcuts import render
from .models import Dishes
from .models import Recipes
from .models import Inventory
from .dishesForm import DishesForm
from .dishesForm import RecipesForm
from django.http import HttpResponse


def dishes_view(request):
    obj = Dishes.objects.get(dishid=1)
    form = DishesForm(request.POST or None)

    if request.POST:
        if '_Delete' in request.POST:
            searchid = request.POST.get('dishid', None)
            name = request.POST.get('name', None)

            Dishes.objects.filter(dishid=searchid).delete()
            return HttpResponse("Dish: " +searchid+ ", " + name + " was deleted")

        elif '_Update' in request.POST:
            searchid = request.POST.get('dishid', None)
            Dishes.objects.filter(dishid=searchid).delete()

    if form.is_valid():
        form.save()

    context = {
        'object': obj,
        'form': form,
    }

    return render(request, "dishes.html", context)


def recipes_view(request):
    form = RecipesForm(request.POST or None)

    if request.POST:

        if '_Update' in request.POST:
            searchid = request.POST.get('dishid', None)
            Dishes.objects.filter(dishid=searchid).delete()

    if form.is_valid():
        form.save()

    context = {
        'form': form,
    }

    return render(request, "recipes.html", context)