from django.db import models


class Inventory(models.Model):
    invid = models.IntegerField(db_column='InvID', primary_key=True)  # Field name made lowercase.
    itemid = models.ForeignKey('Items', models.CASCADE, db_column='ItemID')  # Field name made lowercase.
    datepurchased = models.DateField(db_column='DatePurchased')  # Field name made lowercase.
    shelflife = models.IntegerField(db_column='ShelfLife')  # Field name made lowercase.
    quantity = models.FloatField(db_column='Quantity')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Inventory'


class Supplier(models.Model):
	supplierid = models.IntegerField(db_column='SupplierID', primary_key=True)  # Field name made lowercase.
	name = models.CharField(db_column='Name', max_length=50)  # Field name made lowercase.
	address = models.CharField(db_column='Address', max_length=50)  # Field name made lowercase.
	phoneno = models.CharField(db_column='PhoneNo', max_length=50, blank=True, null=True)  # Field name made lowercase.

	class Meta:
		managed = False
		db_table = 'Supplier'
	def __str__(self):
		return"{},{}".format(self.supplierid,self.name)


class Items(models.Model):
    itemid = models.CharField(db_column='ItemID', primary_key=True,max_length=3)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50)  # Field name made lowercase.
    supplierid = models.ForeignKey('Supplier', models.CASCADE, db_column='SupplierID')  # Field name made lowercase.
    price = models.DecimalField(db_column='Price', max_digits=19, decimal_places=2)  # Field name made lowercase.
    pricetype = models.CharField(db_column='PriceType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    measurementunit = models.CharField(db_column='MeasurementUnit', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Items'


class Dishes(models.Model):
    dishid = models.IntegerField(db_column='DishID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50)  # Field name made lowercase.
    price = models.DecimalField(db_column='Price', max_digits=19, decimal_places=2)  # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Dishes'
    def __str__(self):
        return"{},{}".format(self.dishid,self.name)


class Recipes(models.Model):
    dishid = models.OneToOneField(Dishes, models.CASCADE, db_column='DishID', primary_key=True)  # Field name made lowercase.
    itemid = models.ForeignKey(Items, models.CASCADE, db_column='ItemID')  # Field name made lowercase.
    quantity = models.FloatField(db_column='Quantity')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Recipes'
        unique_together = (('dishid', 'itemid'),)


class Ticket(models.Model):
    ticketid = models.IntegerField(db_column='TicketID', primary_key=True)  # Field name made lowercase.
    date = models.DateField(db_column='Date')  # Field name made lowercase.
    subtotal = models.DecimalField(db_column='Subtotal', max_digits=19, decimal_places=4)  # Field name made lowercase.
    tip = models.DecimalField(db_column='Tip', max_digits=19, decimal_places=4)  # Field name made lowercase.
    total = models.DecimalField(db_column='Total', max_digits=19, decimal_places=4)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Ticket'

class TicketDishes(models.Model):
    ticketid = models.OneToOneField(Ticket, models.CASCADE, db_column='TicketID', primary_key=True)  # Field name made lowercase.
    dishid = models.ForeignKey(Dishes, models.CASCADE, db_column='DishID')  # Field name made lowercase.
    quantity = models.IntegerField(db_column='Quantity')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Ticket_Dishes'
        unique_together = (('ticketid', 'dishid'),)
    def __str__(self):
        return"{},{}".format(self.dishid,self.name)


