from django.shortcuts import render
from .inventoryForm import InventoryForm
from .models import Inventory
from django.http import HttpResponse

def inventory_view(request):
    obj = Inventory.objects.get(invid=3)
    form = InventoryForm(request.POST or None)
    #itemid = ItemsForm()
    if request.POST:
        if '_Delete' in request.POST:
            searchid = request.POST.get('invid', None)
            Inventory.objects.filter(invid=searchid).delete()
            return HttpResponse("Inventory: "+searchid + " was deleted")
        elif '_Update' in request.POST:
            searchid = request.POST.get('invid', None)
            Inventory.objects.filter(invid=searchid).delete()



    if form.is_valid():
        form.save()

    context = {
        'object': obj,
        'form': form,
       # 'itemid': itemid,
    }

    return render(request, "inventory.html", context)