from django import forms
from .models import Inventory


class InventoryForm(forms.ModelForm):
    class Meta:
        model = Inventory
        fields = [
            'invid',
            'itemid',
            'datepurchased',
            'shelflife',
            'quantity'
        ]
        widgets = {
            'invid': forms.TextInput(attrs={'class': 'input'}),
            'itemid': forms.TextInput(attrs={'class': 'input'}),
            'datepurchased': forms.TextInput(attrs={'class': 'input'}),
            'shelflife': forms.TextInput(attrs={'class': 'input'}),
            'quantity': forms.TextInput(attrs={'class': 'input'}),
        }