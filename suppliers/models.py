from django.db import models


class Supplier(models.Model):
    supplierid = models.IntegerField(db_column='SupplierID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=50)  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=50)  # Field name made lowercase.
    phoneno = models.CharField(db_column='PhoneNo', max_length=50, blank=True, null=True)  # Field name made lowercase.
		
    class Meta:
        managed = False
        db_table = 'Supplier'
		
	
		
	