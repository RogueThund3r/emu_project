from django.shortcuts import render
from .models import Supplier
from .suppliersForm import SuppliersForm
from django.http import HttpResponse


def suppliers_view(request):
    
    form = SuppliersForm(request.POST or None)

    if request.POST:
        if '_Delete' in request.POST:
            searchid = request.POST.get('supplierid', None)
            Supplier.objects.filter(supplierid=searchid).delete()
            return HttpResponse("Supplier: "+searchid + " was deleted")
        elif '_Update' in request.POST:
            searchid = request.POST.get('supplierid', None)
            Supplier.objects.filter(supplierid=searchid).delete()



    if form.is_valid():
        form.save()

    context = {
        
        'form': form,
    }

    return render(request, "suppliers.html", context)