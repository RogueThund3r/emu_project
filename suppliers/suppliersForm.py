from django import forms
from .models import Supplier


class SuppliersForm(forms.ModelForm):
    class Meta:
        model = Supplier
        fields = [
            'supplierid',
            'name',
            'address',
            'phoneno',
        ]

        widgets = {
            'supplierid': forms.TextInput(attrs={'class': 'input'}),
            'name': forms.TextInput(attrs={'class': 'input'}),
            'address': forms.TextInput(attrs={'class': 'input'}),
            'phoneno': forms.TextInput(attrs={'class': 'input'}),
        }
